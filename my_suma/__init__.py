def sum(arg):
    total = 0
    for val in arg:
        total += val
    return total

def calculadora(a,b,c):
    switcher = {
        "+": a + b,
        "-": a - b
    }
    return switcher.get(c, -0)