import unittest

from my_suma import sum

from my_suma import calculadora

class TestSum(unittest.TestCase):
    def test_list(self):
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6)
    
    def test_list_dos(self):
        result = calculadora(5,5,'+')
        self.assertEqual(result, 10)

        result = calculadora(10,5,'-')
        self.assertEqual(result, 5)

if __name__ == '__main__':
    unittest.main()